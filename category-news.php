<?php get_header(); ?>


<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h1>LES NOUVELLES DU FESTIVAL</h1>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-8">
        <ul class="list-article"> 

          <?php
          if ( have_posts() ) :
          while ( have_posts() ) : the_post();?>
          <a href="<?php the_permalink() ?>" >
            <li class="conteneur">
              <div style="background-image:url(<?php if ( has_post_thumbnail() ) {the_post_thumbnail_url();}?>)" class="photo-article">
              </div>
              <div class="text-article">
                <h2><?php the_title() ?></h2>
                <p><?php the_excerpt() ?></p>
              </div>
            </li>
          </a>
          <?php 
  endwhile;
                  endif;
          ?>

        </ul>
      </div>
      <div class="col-lg-4">
        <div class="pub" style="background-image:url(../img/bandeau/bandeau1.jpg)">
        </div>
        <div class="pub" style="background-image:url(../img/bandeau/bandeau4.jpg)">
        </div>
        <div class="pub" style="background-image:url(../img/bandeau/bandeau2.jpg)">
        </div>
        <div class="pub" style="background-image:url(../img/bandeau/bandeau3.jpg)">
        </div>
      </div>
    </div>

  </div>
</section>

<?php get_footer();