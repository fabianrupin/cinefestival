<?php
if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}

//logo
add_theme_support('custom-logo', array(
    'height' => 100,
    'width' => 400,
    'flex-height' => true,
    'flex-width' => true,
    'header-text' => array('site-title', 'site-description'),
));

add_action('init', 'zero_add_menu');
function zero_add_menu()
{
    register_nav_menu('main_menu', 'Menu principal');
    register_nav_menu('footer_menu', 'Menu de footer');
}

function bd_parse_post_variables()
{
    // bd_parse_post_variables function for WordPress themes by Nick Van der Vreken.
    // please refer to bydust.com/using-custom-fields-in-wordpress-to-attach-images-or-files-to-your-posts/
    // for further information or questions.
    global $post, $post_var;

    // fill in all types you'd like to list in an array, and
    // the label they should get if no label is defined.
    // example: each file should get label "Download" if no
    // label is set for that particular file.
    $types = array('image' => 'no info available',
        'file' => 'Download',
        'link' => 'Read more...');

    // this variable will contain all custom fields
    $post_var = array();
    foreach (get_post_custom($post->ID) as $k => $v) $post_var[$k] = array_shift($v);

    // creating the arrays
    foreach ($types as $type => $message) {
        global ${'post_' . $type . 's'}, ${'post_' . $type . 's_label'};
        $i = 1;
        ${'post_' . $type . 's'} = array();
        ${'post_' . $type . 's_label'} = array();
        while ($post_var[$type . $i]) {
            //echo $type.$i.' - '.${$type.$i.'_label'};
            array_push(${'post_' . $type . 's'}, $post_var[$type . $i]);
            array_push(${'post_' . $type . 's_label'}, $post_var[$type . $i . '_label'] ? htmlspecialchars($post_var[$type . $i . '_label']) : $message);
            $i++;
        }
    }
}

if (function_exists('register_sidebar')) {

    register_sidebar(array(
        'name' => 'Widgetized Area',
        'id' => 'widgetized-area',
        'description' => 'This is a widgetized area.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ));

}

// Our custom post type function
function create_posttype()
{

    register_post_type('movies',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Movies'),
                'singular_name' => __('Movie')
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'movies'),
        )
    );
}

// Hooking up our function to theme setup
add_action('init', 'create_posttype');

/*
* Creating a function to create our CPT
*/

function custom_post_type()
{

// Set UI labels for Custom Post Type
    $labels = array(
        'name' => _x('Movies', 'Post Type General Name', 'cinefestival'),
        'singular_name' => _x('Movie', 'Post Type Singular Name', 'cinefestival'),
        'menu_name' => __('Movies', 'cinefestival'),
        'parent_item_colon' => __('Parent Movie', 'cinefestival'),
        'all_items' => __('All Movies', 'cinefestival'),
        'view_item' => __('View Movie', 'cinefestival'),
        'add_new_item' => __('Add New Movie', 'cinefestival'),
        'add_new' => __('Add New', 'cinefestival'),
        'edit_item' => __('Edit Movie', 'cinefestival'),
        'update_item' => __('Update Movie', 'cinefestival'),
        'search_items' => __('Search Movie', 'cinefestival'),
        'not_found' => __('Not Found', 'cinefestival'),
        'not_found_in_trash' => __('Not found in Trash', 'cinefestival'),
    );

// Set other options for Custom Post Type

    $args = array(
        'label' => __('movies', 'cinefestival'),
        'description' => __('entités films permettant la description d\'un film', 'cinefestival'),
        'labels' => $labels,
        // Features this CPT supports in Post Editor
        'supports' => array('title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields',),
        // You can associate this CPT with a taxonomy or custom taxonomy.
        'taxonomies' => array('genres'),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 5,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    );

    // Registering your Custom Post Type
    register_post_type('movies', $args);

}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action('init', 'custom_post_type', 0);

?>