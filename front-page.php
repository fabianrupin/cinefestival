<?php get_header() ?>

<section class="homepage-section pair">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-push-2 text-justify">
        <h2>L'ESPRIT DU FESTIVAL</h2>
        <p>Devenu un événement incontournable du cinéma français et européen, le Festival accueille plus de 12
          000 festivaliers pendant 5 jours pour des projections en salles ou en plein air, de films ayant pour
          thème central le Romantisme. La passion, l’amour et la rêverie sont au cœur du récit des films
          présentés.</p>
        <div class="row">
          <div class="col-lg-12 text-center">
            <a href="<?php bloginfo('url') ?>/le-festival" class="btn btn-primary">En savoir plus</a>
          </div>
        </div>

        <figure class="img-section">
          <img src=<?php echo get_template_directory_uri() ?>/img/zcecile-de-france-swann.jpg alt="cecile-de-france" class="img-full-width-section ">
        </figure>
      </div>
    </div>
  </div>
</section>

<section class="homepage-section impair">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-push-2 col-md-10 col-md-push-1 col-sm-12 text-justify">
        <h2>LES ACTUALITES DU FESTIVAL</h2>
        <p>Retrouvez toute l'actualité du festival, les prochains films, les avis des spectateurs, </p>
      </div>
    </div>
    <div class="row">
      <?php query_posts('cat="films"&posts_per_page=4'); ?>
      <?php
  if ( have_posts() ) :
while ( have_posts() ) : the_post();
if (in_category('Films')) continue; 
      ?>
      <div class="col-lg-3 col-md-6 col-sm-12">
        <a href="<?php the_permalink() ?>" >
          <div class="rounded-image">
            <img src="<?php if ( has_post_thumbnail() ) {the_post_thumbnail_url();} ?>" alt="panorama-rogne">
          </div>
          <h4><?php the_title() ?></h4>
          <p><?php the_excerpt() ?></p>
        </a>
      </div>
      <?php 
        endwhile;
      endif;
      ?>

      <div class="col-lg-12 text-center">
        <a href="<?php bloginfo('url') ?>/category/news" class="btn btn-primary">Retrouvez toutes les actualités du festival</a>
      </div>

    </div>
  </div>
</section>

<section class="homepage-section pair">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-push-2 col-md-12 text-justify">
        <h2>LES PROCHAINES PROJECTIONS</h2>
        <p>En plus d'une cohérence, c'est une véritable expérience qui vous feront voyager de la naissance à
          l'adolescence, de la joie de l'aventure à la colère des puissants.</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 col-lg-push-2">
        <ul class="nav nav-pills nav-justified">
          <li class="active"><a href="#panorama" data-toggle="tab">Panorama</a></li>
          <li><a href="#premier-amour" data-toggle="tab">Premier Amour</a></li>
          <li><a href="#la-route" data-toggle="tab">La Route</a></li>
          <li><a href="#colere-et-empire" data-toggle="tab">Colère et Empire</a></li>
        </ul>
      </div>
    </div>
    <div class="row tab-content">
      <div class="tab-pane active fade in" id="panorama">
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/lepapillon.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">LE PAPILLON<span
                                                                                                   class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/mud.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">MUD<span
                                                                                           class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/septanstibet.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">7 ANS AU TIBET<span
                                                                                                      class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="premier-amour">
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/amour.jpeg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">AMOUR<span
                                                                                             class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/romeo+juliette.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">ROMEO + JULIETTE<span
                                                                                                        class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/brockeback.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">BROKEBACK MOUNTAIN<span
                                                                                                          class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="la-route">
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/surlaroute.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">SUR LA ROUTE<span
                                                                                                    class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/littlemisssunshine.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">LITTLE MISS
                SUNSHINE<span class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/route2.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">LA ROUTE<span
                                                                                                class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade" id="colere-et-empire">
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/300.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">300<span
                                                                                           class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/pompei.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">POMPEI<span
                                                                                              class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-body" style="background-image:url(<?php echo get_template_directory_uri() ?>/img/gladiator.jpg)">
            </div>
            <div class="panel-footer">
              <a class="btn btn-block btn-lg btn-default" href="views/films.html">GLADIATOR <span
                                                                                                  class="glyphicon glyphicon-chevron-right"></span></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php get_footer() ?>
