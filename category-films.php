<?php get_header(); ?>


<section id="programmation">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div class="panel">
          <h1>TOUS LES FILMS DU FESTIVAL</h1>
          <p>Retrouvez ici tous les films du festival des films de Pleine Air, au Parc Monceaux.</p>
          <p>Toutes les séances commencent à la tombée de la nuit, soit : </p>
          <ul>
            <li>à partir de 23h au début du festival</li>
            <li>à partir de 22H30 au milieu du festival</li>
            <li>à partir de 22H à la fin du festival</li>
          </ul>
          <p>Tous les films diffusés pendant le festival sont gratuits et accessibles à tous. Les places étant limités, nous vous conseillons d'arriver sur le site au moins 45 minutes avant le début du film</p>
          <p>Et surtout, vive le cinéma !</p>
        </div>
      </div>


      <?php
      if ( have_posts() ) :
      /* Start the Loop */
      while ( have_posts() ) : the_post();?>

<!--      <?php var_dump( get_post_meta($post->ID,'Réalisation',true));?>-->

      <div class="col-sm-4">
        <a href=<?php the_permalink(); ?>>
      <div class="panel" style="background-image:url(<?php if ( has_post_thumbnail() ) {the_post_thumbnail_url();}?> )">
        <div class="panel-body">
        </div>
        <div class="panel-footer">
          <table class="table table-condensed">
            <tr>
              <th>Réalisation</th>
              <th>Date de sortie</th>
            </tr>
            <tr>
              <td>
                <?php echo get_post_meta($post->ID,'Réalisation',true) ?>
                
              </td>
              <td><?php echo get_post_meta($post->ID,'Date de sortie',true) ?></td>
            </tr>
          </table>
        </div>
      </div>
      </a>
  </div>

  <?php endwhile;
  endif;
  ?>

  </div>
</div>
</section>

<?php get_footer();