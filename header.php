<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js" >
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php wp_title('', true,''); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
    <script src="<?php bloginfo('template_url') ?>/js/vendor/modernizr-2.8.3.min.js"></script>
    <?php wp_head(); ?>
  </head>

  <body>
    <?php if (is_front_page()): ?>
    <header id="couverture" class="impair">
      <div class="container">
        <nav class="row">
          <div class="col-lg-1 col-md-3 col-sm-5 col-xs-1">
            <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
            $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );?>
            <a href="<?php bloginfo('url') ?>"><img class="logo" src="<?php echo $logo[0] ?>" alt="logo"></a>
          </div>
          <div class="col-lg-11 col-md-9 col-sm-7 col-xs-11">

            <div class="navbar-default navbar-right" role="navigation">
              <div class="navbar-header">   
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <?php wp_nav_menu(array('theme_location' => 'main_menu','container_class' => "collapse navbar-collapse", "menu_class" => "nav navbar-nav")); ?>
            </div>
          </div>
        </nav>

        <div class="row">
          <div class="col-lg-5 col-md-7 col-sm-9 col-xs-11">
            <h1 class="home-title"><?php bloginfo('name') ?></h1>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-5 col-md-7 col-sm-9 col-xs-12">
            <p>PARC MONCEAUX - 5 AU 8 AOUT 2017</p>
          </div>
        </div>
      </div>
    </header>

    <?php else: ?>
    <header>
      <div class="container">
        <nav class="row">
          <div class="col-lg-1 col-md-3 col-sm-5 col-xs-1">
            <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
            $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );?>
            <a href="<?php bloginfo('url') ?>"><img class="logo" src="<?php echo $logo[0] ?>" alt="logo"></a>
          </div>
          <div class="col-lg-11 col-md-9 col-sm-7 col-xs-11">
            <div class="navbar-default navbar-right" role="navigation">
              <div class="navbar-header">   
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
              </div>
              <?php wp_nav_menu(array('theme_location' => 'main_menu','container_class' => "collapse navbar-collapse", "menu_class" => "nav navbar-nav")); ?>
            </div>
          </div>
        </nav>
      </div>
    </header>
    <?php endif; ?>

