<?php get_header(); ?>

<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-xs-12">
        <?php
        while ( have_posts() ) : the_post();

        the_content();

        endwhile; // End of the loop.
        ?>


      </div>
      <div class="col-sm-6 col-xs-12">
        <h1>POUR VENIR</h1>
        <p>Le festival est situé en face du lac du parc Monceau, l'entrée est libre mais il vous sera demandé de vous soumettre à la fouille pour accéder au parc. </p>
        <?php echo do_shortcode('[wpgmza id="1"]'); ?>
      </div>
    </div>
  </div>
</section>
<?php get_footer();