<?php get_header(); ?>

<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <p>
            <?php the_title() . "<br />"; ?>
            <?php the_content() . "<br />"; ?>
            <?php the_date() . " - " . the_time(); ?>
        </p>
    <?php endwhile; ?>
<?php endif; ?>

<?php get_sidebar() ?>

<?php get_footer(); ?>