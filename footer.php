<footer class="impair">
  <div class="container">
    <div class="row">
      <div class="col-lg-4">
        <?php wp_nav_menu(array('theme_location' => 'footer_menu','container_class' => "collapse navbar-collapse", "menu_class" => "nav navbar-nav")); ?>
      </div>

      <div class="col-lg-push-4 col-lg-4">
        <table class="table text-right">
          <tbody>
            <tr>
              <td>Fait à Saint-Denis par Fabian Rupin</td>
            </tr>
            <tr>
              <td>Copyright 2017</td>
            </tr>
          </tbody>
        </table>
      </div>

    </div>
  </div>
</footer>


<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
<script src="<?php bloginfo('template_url') ?>/js/plugins.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/main.js"></script>
<script src="<?php bloginfo('template_url') ?>/js/bootstrap.js"></script>

<!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
<script>
  (function (b, o, i, l, e, r) {
    b.GoogleAnalyticsObject = l;
    b[l] || (b[l] =
             function () {
      (b[l].q = b[l].q || []).push(arguments)
    });
    b[l].l = +new Date;
    e = o.createElement(i);
    r = o.getElementsByTagName(i)[0];
    e.src = 'https://www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e, r)
  }(window, document, 'script', 'ga'));
  ga('create', 'UA-XXXXX-X', 'auto');
  ga('send', 'pageview');
</script>
<?php wp_footer() ?>
</body>
</html>