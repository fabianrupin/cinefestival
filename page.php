<?php get_header(); ?>

<section id="description-festival" class="pair">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-lg-push-2 text-justify">

        <?php
        while ( have_posts() ) : the_post();

        the_content();

        endwhile; // End of the loop.
        ?>

      </div>
    </div>
  </div>
</section>

<?php get_footer();