<?php get_header();
while ( have_posts() ) : the_post();
bd_parse_post_variables();
?>
<!-- Caroussel
================================================== -->
<div id="monCarousel" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#monCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#monCarousel" data-slide-to="1"></li>
    <li data-target="#monCarousel" data-slide-to="2"></li>
  </ol>


  <!-- Diapositives -->
  <div class="carousel-inner" >
    <div class="carousel-caption caption-infos-movie">

      <h1><?php the_title(); ?></h1>
      <p>par <?php the_author_firstname()?> <?php the_author_lastname() ?> - <?php the_date() ?></p>

    </div>

    <div class="carousel-caption breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php bloginfo('url') ?>">Accueil</a></li>
        <li class="breadcrumb-item"><a href="<?php bloginfo('url') ?>/news">Actualités</a></li>
        <li class="breadcrumb-item active"><?php the_title() ?></li>
      </ol>
    </div>

    <div class="item active">
      <img src="<?php bloginfo('url') ?>/wp-content/uploads/<?php echo $post_images[0] ?>" alt="<?php echo $post_images_label[0] ?>" width="1600" height="900">
    </div>
    <div class="item">
      <img src="<?php bloginfo('url') ?>/wp-content/uploads/<?php echo $post_images[1] ?>" alt="<?php echo $post_images_label[1] ?>" width="1600" height="900">
    </div>
    <div class="item">
      <img src="<?php bloginfo('url') ?>/wp-content/uploads/<?php echo $post_images[2] ?>" alt="<?php echo $post_images_label[2] ?>" width="1600" height="900">
    </div>
  </div>
</div>

<section class="container text-justify">
  <article class="row news-article">
    <div class="col-lg-push-2 col-lg-8">
      <h1><?php the_title(); ?></h1>
      <?php the_content(); ?>

    </div>
  </article>
</section>

<?php endwhile;?>

<?php get_footer();