<?php get_header();
while (have_posts()) : the_post();
    bd_parse_post_variables();
    ?>
    <!-- Caroussel
    ================================================== -->
    <div id="monCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#monCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#monCarousel" data-slide-to="1"></li>
            <li data-target="#monCarousel" data-slide-to="2"></li>
        </ol>


        <!-- Diapositives -->
        <div class="carousel-inner">
            <div class="carousel-caption caption-infos-movie">

                <h1><?php the_title(); ?></h1>
                <p>Un film de <?php echo get_post_meta($post->ID, 'Réalisation', true) ?></p>
                <p>Parc Monceaux - <?php echo get_post_meta($post->ID, 'Date', true) ?></p>
                <p><a class="btn  btn-primary" href="#registerForm" role="button">Réserver dès maintenant</a></p>
            </div>

            <div class="carousel-caption breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php bloginfo('url') ?>">Accueil</a></li>
                    <li class="breadcrumb-item"><a href="<?php bloginfo('url') ?>/movies">Programmation</a></li>
                    <li class="breadcrumb-item active"><?php the_title() ?></li>
                </ol>
            </div>

            <div class="item active">
                <img src="<?php echo $post_images[0] ?>" alt="<?php echo $post_images_label[0] ?>" width="1600"
                     height="900">
            </div>
            <div class="item">
                <img src="<?php echo $post_images[1] ?>" alt="<?php echo $post_images_label[1] ?>" width="1600"
                     height="900">
            </div>
            <div class="item">
                <img src="<?php echo $post_images[2] ?>" alt="<?php echo $post_images_label[2] ?>" width="1600"
                     height="900">
            </div>
        </div>
    </div>


    <section class="container text-justify">
        <article class="row news-article">
            <div class="col-lg-push-2 col-lg-8">
                <h1><?php the_title(); ?></h1>
                <?php the_content(); ?>

            </div>
        </article>
    </section>

    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <img class="affiche-film " src="../img/affiche/laroute.jpg" alt="affiche-la-route">
            </div>
            <div class="col-lg-8">
                <h1><?php the_title(); ?></h1>

                <h2>SEANCE</h2>
                <p class="text-justify movie-paragraph"><?php echo get_post_meta($post->ID, 'Date', true) ?> dans la cour principale du parc Monceau</p>
                <h2>SYNOPSIS</h2>
                <p class="text-justify movie-paragraph"><?php the_content() ?></p>
                <h2>DETAILS</h2>
                <table class="table table-striped table-condensed movie-paragraph">

                    <tr>
                        <th>Réalisation</th>
                        <th>date de sortie</th>
                        <th>Genre</th>
                        <th>Nationalité</th>
                    </tr>

                    <tr>
                        <th><?php echo get_post_meta($post->ID, 'Réalisation', true) ?></th>
                        <th><?php echo get_post_meta($post->ID, 'Date de sortie', true) ?></th>
                        <th><?php echo get_post_meta($post->ID, 'Genre', true) ?></th>
                        <th><?php echo get_post_meta($post->ID, 'Nationalité', true) ?></th>
                    </tr>

                </table>

                <h2 id="registerForm">INSCRIPTION</h2>
                <form class="movie-paragraph">
                    <div class="form-group">
                        <label for="prenom">Prénom</label>
                        <input id="prenom" type="text" class="form-control" placeholder="Entrez votre prénom ici..."
                               required="true">
                    </div>
                    <div class="form-group">
                        <label for="nom">Nom</label>
                        <input id="nom" type="text" class="form-control" placeholder="Entrez votre nom ici..."
                               required="true">
                    </div>
                    <div class="form-group">
                        <label for="telephone">Numéro de téléphone</label>
                        <input id="telephone" type="phone" class="form-control"
                               placeholder="Entrez votre numéro de téléphone">
                    </div>
                    <div class="form-group">
                        <label for="adresse">Adresse</label>
                        <input id="adresse" type="text" class="form-control"
                               placeholder="Entrez votre adresse postale...">
                    </div>
                    <div class="form-group">
                        <label for="email">Adresse Email</label>
                        <input id="email" type="email" class="form-control" placeholder="Rentrez votre mail ici..."
                               required="true">
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox">Envoyez moi un mail de confirmation
                        </label>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Envoyer</button>
                    </div>

                </form>


            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php get_footer();